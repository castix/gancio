import it from './it.json'
import en from './en.json'
import es from './es.json'
import ca from './ca.json'

export default {
  it, en, es, ca
}
